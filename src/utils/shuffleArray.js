/**
 * Randomly reorder array elements
 * @param {Array} array array whose elements to shuffle
 * @returns {Array} array with shuffled elements
 */
export default function shuffleArray(array) {
  if (array.length === 0) {
    return array;
  }
  let newArray = array.slice(0);
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [newArray[i], newArray[j]] = [newArray[j], newArray[i]];
  }
  return newArray;
}
