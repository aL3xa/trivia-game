import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import dompurify from 'dompurify';

const SanitizeHTML = forwardRef(({ html, sanitizeOptions }, ref) => (
  <span
    ref={ref}
    dangerouslySetInnerHTML={{
      __html: dompurify.sanitize(html, sanitizeOptions),
    }}
  ></span>
));

SanitizeHTML.propTypes = {
  html: PropTypes.string.isRequired,
  sanitizeOptions: PropTypes.object,
};

SanitizeHTML.defaultProps = {
  sanitizeOptions: {},
};

export default SanitizeHTML;
