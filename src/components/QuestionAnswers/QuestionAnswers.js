import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { shuffleArray } from 'utils';
import { SanitizeHTML } from 'components';
import { Flex } from 'ui';
import { AnswerButton } from './styled';
import { UI_QUIZ_GOTO_NEXT_QUESTION_TIMEOUT } from 'consts';

function QuestionAnswers({
  type,
  correct_answer,
  incorrect_answers,
  onAnswer,
  onNextQuestion,
  showCorrect,
  shouldRender,
}) {
  const answers = useMemo(() => {
    if (type === 'boolean') {
      return ['True', 'False'];
    }
    return shuffleArray([correct_answer, ...incorrect_answers]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);
  const [userAnswer, setUserAnswer] = useState(null);
  const [shouldSlideIn, setShouldSlideIn] = useState(false);
  const [timerId, setTimerId] = useState(null);
  const [preventAnswering, setPreventAnswering] = useState(true);

  function answerStyle(answer) {
    if (showCorrect === false || userAnswer === null) {
      return;
    }
    if (answer === correct_answer) {
      return 'success';
    }
    if (answer === userAnswer) {
      return 'error';
    }
  }

  function giveAnswer(answer) {
    return (evt) => {
      if (preventAnswering || userAnswer !== null) {
        evt.preventDefault();
        return;
      }
      setUserAnswer(answer);
      setTimerId(
        setTimeout(
          () => {
            setShouldSlideIn(false);
          },
          showCorrect ? UI_QUIZ_GOTO_NEXT_QUESTION_TIMEOUT : 0
        )
      );
      onAnswer({
        answer,
        isCorrect: answer === correct_answer,
      });
    };
  }

  const handleTransitionEnd = (id) => (evt) => {
    const isLastAnswer = id === answers.length - 1;
    if (isLastAnswer) {
      setPreventAnswering(false);
    }
    if (userAnswer !== null && isLastAnswer) {
      onNextQuestion();
    }
  };

  useEffect(() => {
    if (shouldRender) {
      setShouldSlideIn(true);
    }
  }, [shouldRender]);

  useEffect(() => {
    return () => {
      clearTimeout(timerId);
    };
  }, [timerId]);

  // ensure buttons are unlocked if transitions don't occur
  useEffect(() => {
    let id;
    if (preventAnswering) {
      id = setTimeout(() => {
        setPreventAnswering(false);
      }, 300);
    }
    return () => {
      clearInterval(id);
    };
  }, [preventAnswering]);

  return (
    shouldRender && (
      <Flex
        flex={1}
        flexDirection="column"
        overflow="hidden"
        justifyContent="flex-end"
      >
        {answers.map((answer, i) => (
          <AnswerButton
            key={i}
            mb={1}
            variant={answerStyle(answer)}
            onClick={giveAnswer(answer)}
            onTransitionEnd={handleTransitionEnd(i)}
            shouldSlideIn={shouldSlideIn}
            isAnswered={!!userAnswer}
            isCorrect={answer === correct_answer}
            isUserAnswer={answer === userAnswer}
            showCorrect={showCorrect}
          >
            <SanitizeHTML html={answer} />
          </AnswerButton>
        ))}
      </Flex>
    )
  );
}

QuestionAnswers.propTypes = {
  type: PropTypes.oneOf(['boolean', 'multiple']).isRequired,
  correct_answer: PropTypes.string.isRequired,
  incorrect_answers: PropTypes.arrayOf(PropTypes.string).isRequired,
  onAnswer: PropTypes.func.isRequired,
  onNextQuestion: PropTypes.func.isRequired,
  showCorrect: PropTypes.bool,
  shouldRender: PropTypes.bool,
};

QuestionAnswers.defaultProps = {
  showCorrect: true,
  shouldRender: true,
};

export default QuestionAnswers;
