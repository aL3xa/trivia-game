import styled, { css } from 'styled-components';
import { Button } from 'ui';

const answeredBgColor = ({ theme, variant, isAnswered, showCorrect }) => {
  if (!showCorrect) return;
  if (!isAnswered) return;

  const col = theme.palette[variant]?.main || theme.colors.gray;
  return css`
    &:hover,
    &:active {
      background-color: ${col};
    }
  `;
};

const pulseCorrect = ({
  theme,
  isAnswered,
  isCorrect,
  isUserAnswer,
  showCorrect,
}) => {
  if (!showCorrect) return;

  if (isAnswered && isCorrect) {
    return css`
      animation: pulse-correct ${isUserAnswer ? 0.4 : 0.2}s ease-in-out
        ${isUserAnswer ? 1 : 3};

      @keyframes pulse-correct {
        0% {
          background-color: ${theme.palette.success.main};
        }
        50% {
          background-color: ${theme.palette.success.light};
        }
        100% {
          background-color: ${theme.palette.success.main};
        }
      }
    `;
  }
};

const slideIn = ({ shouldSlideIn }) =>
  css`
    transition: transform 0.15s ease-in-out, opacity 0.2s linear 0.1s;
    opacity: ${shouldSlideIn ? 1 : 0.5};
    transform: translateX(${shouldSlideIn ? 0 : '-101%'});

    &:nth-of-type(2) {
      transition-delay: 0.1s;
    }
    &:nth-of-type(3) {
      transition-delay: 0.15s;
    }
    &:nth-of-type(4) {
      transition-delay: 0.2s;
    }
  `;

export const AnswerButton = styled(Button)(
  css`
    user-select: none;
    box-shadow: none;
    &:hover,
    &:active {
      box-shadow: none;
    }
  `,
  slideIn,
  pulseCorrect,
  answeredBgColor
);
