import React from 'react';
import PropTypes from 'prop-types';
import { SanitizeHTML } from 'components';
import { Box, Text, Flex } from 'ui';
import { DifficultyText } from './styled';

function QuestionCard({ category, difficulty, question, children }) {
  return (
    <Flex
      position="relative"
      height="100%"
      width="100%"
      flexDirection="column"
      justifyContent="space-between"
    >
      <Text variant="h3" fontWeight="bold" textAlign="center" my={[5, 3, 3]}>
        {category}
      </Text>

      <DifficultyText
        difficulty={difficulty}
        position="absolute"
        top={0}
        right={0}
        p={1}
        mt={[0, -3, -3]}
        fontSize={1}
      >
        {difficulty}
      </DifficultyText>

      <Text
        fontSize={3}
        my={1}
        px={[0, 3, 5]}
        textAlign="center"
        style={{ wordBreak: 'break-word' }}
      >
        <SanitizeHTML html={question} />
      </Text>

      <Box flexDirection="column" alignItems="center">
        {children}
      </Box>
    </Flex>
  );
}

QuestionCard.propTypes = {
  category: PropTypes.string.isRequired,
  difficulty: PropTypes.string,
  question: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

QuestionCard.defaultProps = {};

export default QuestionCard;
