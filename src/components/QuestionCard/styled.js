import styled, { css } from 'styled-components';
import { Text } from 'ui';

const difficultyBgCol = ({ theme, difficulty }) =>
  theme.colors[
    {
      easy: 'success',
      medium: 'primary',
      hard: 'error',
    }[difficulty]
  ];

export const DifficultyText = styled(Text)(
  ({ theme, difficulty }) => css`
    color: ${theme.colors.white};
    background-color: ${difficultyBgCol({ theme, difficulty })};
  `
);
