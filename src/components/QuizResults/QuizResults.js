import React from 'react';
import PropTypes from 'prop-types';
import { SanitizeHTML } from 'components';
import { Box, Text, Button, Flex } from 'ui';
import { Close as IconWrong, Check as IconCorrect } from 'ui/icons';

function QuizResults({ score, questions, answers, onPlayAgain }) {
  return (
    <Flex
      height="100%"
      alignItems="center"
      flexDirection="column"
      justifyContent="space-around"
    >
      <Text variant="h3" fontWeight="bold" my={3} textAlign="center">
        You scored {Math.round((score / questions.length) * 100)}%
      </Text>
      <Box textAlign="left" mb={1} px={2} overflow="auto" width={[1, 0.8, 0.8]}>
        {answers.map(({ isCorrect }, i) => (
          <Flex key={i} display="flex" py={1} alignItems="center">
            {isCorrect ? (
              <IconCorrect fill="green" />
            ) : (
              <IconWrong fill="red" />
            )}
            <Text flex={1} ml={3}>
              <SanitizeHTML html={questions[i].question} />
            </Text>
          </Flex>
        ))}
      </Box>
      <Button variant="primary" my={3} onClick={onPlayAgain}>
        Play again?
      </Button>
    </Flex>
  );
}

QuizResults.propTypes = {
  score: PropTypes.number.isRequired,
  questions: PropTypes.array.isRequired,
  answers: PropTypes.array.isRequired,
  onPlayAgain: PropTypes.func.isRequired,
};

export default QuizResults;
