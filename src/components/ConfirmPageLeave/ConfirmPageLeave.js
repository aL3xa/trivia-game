import React, { useEffect } from 'react';
import { Prompt } from 'react-router-dom';
import PropTypes from 'prop-types';

function ConfirmPageLeave({ when, message }) {
  useEffect(() => {
    const eventName = 'beforeunload';
    const eventHandler = (evt) => {
      if (when) {
        evt.preventDefault(); // FF
        evt.returnValue = ''; // Chrome
      }
    };

    window.addEventListener(eventName, eventHandler, false);

    return () => {
      window.removeEventListener(eventName, eventHandler, false);
    };
  }, [when]);

  return <Prompt when={when} message={message} />;
}

ConfirmPageLeave.propTypes = {
  when: PropTypes.bool,
  message: PropTypes.string,
};

ConfirmPageLeave.defaultProps = {
  when: false,
  message: 'Are you sure?',
};

export default ConfirmPageLeave;
