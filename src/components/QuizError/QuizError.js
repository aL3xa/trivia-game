import React from 'react';
import { Link } from 'react-router-dom';
import { Box, Text, Button } from 'ui';

function QuizError({ error }) {
  return (
    <Box textAlign="center">
      <Text variant="h3" color="error" fontWeight="bold" my={3}>
        Quiz error
      </Text>
      {error === '1' ? (
        <>
          <Text>
            No questions found in this category. Please go to settings and try
            changing your preferences.
          </Text>
          <Link to="/settings">
            <Button mt={3} variant="primary">
              Open settings
            </Button>
          </Link>
        </>
      ) : (
        <Text>Please try again.</Text>
      )}
    </Box>
  );
}

export default QuizError;
