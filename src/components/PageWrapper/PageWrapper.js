import React from 'react';
import styled, { css } from 'styled-components';
import { Flex } from 'ui';

const StyledPage = styled(Flex)(
  ({ theme }) => css`
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);
  `
);

function PageWrapper({ children }) {
  return (
    <StyledPage
      p={3}
      width={[1, 1 / 2, 600]}
      maxWidth={600}
      height={['100%', '80%', '70%']}
      alignItems="center"
      justifyContent="center"
    >
      {children}
    </StyledPage>
  );
}

export default PageWrapper;
