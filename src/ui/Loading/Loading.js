import React from 'react';
import PropTypes from 'prop-types';
import { Container, StyledDot } from './styled';

function Loading({ size, ...otherProps }) {
  return (
    <Container size={size} {...otherProps}>
      <StyledDot />
      <StyledDot />
      <StyledDot />
    </Container>
  );
}

Loading.propTypes = {
  size: PropTypes.number,
};

Loading.defaultProps = {
  size: 15,
};

export default Loading;
