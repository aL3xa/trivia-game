import styled, { css } from 'styled-components';
import { Box } from '../';

const dotSize = 15;
export const Container = styled(Box)(
  ({ size = dotSize }) => css`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    font-size: size;
    width: ${size * 4}px;
  `
);

export const StyledDot = styled.span(
  ({ theme: { color }, size = dotSize }) => css`
    height: ${size}px;
    width: ${size}px;
    border-radius: 100%;
    background-color: ${color};
    display: inline-block;
    animation: pulse 1.5s ease-in-out infinite;

    &:nth-of-type(2) {
      animation-delay: 0.25s;
    }
    &:nth-of-type(3) {
      animation-delay: 0.5s;
    }

    @keyframes pulse {
      0% {
        opacity: 0.3;
      }
      20% {
        opacity: 1;
      }
      100% {
        opacity: 0.3;
      }
    }
  `
);
