import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { StyledIconButton } from './styled';

const IconButton = forwardRef(({ children, ...otherProps }, ref) => {
  return (
    <StyledIconButton ref={ref} {...otherProps}>
      {children}
    </StyledIconButton>
  );
});

IconButton.propTypes = {
  /** Button size */
  size: PropTypes.oneOf(['small', 'medium', 'large']),
};

IconButton.defaultProps = {
  size: 'medium',
};

export default IconButton;
