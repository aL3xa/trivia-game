import styled from 'styled-components';
import { composedStyledSystem } from 'ui/utils';
import { Button } from '../';

const baseStyles = () => ({
  borderRadius: '100%',
  display: 'flex',
});

const sizeStyles = ({ theme: { space }, size }) =>
  ({
    small: {
      padding: `${space[1]}px`,
    },
    medium: {
      padding: `${space[2]}px`,
    },
    large: {
      padding: `${space[3]}px`,
    },
  }[size]);

export const StyledIconButton = styled(Button)(
  baseStyles,
  sizeStyles,
  composedStyledSystem
);
