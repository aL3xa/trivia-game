import React, { forwardRef } from 'react';
import { StyledFlex } from './styled';

const Flex = forwardRef(({ children, ...otherProps }, ref) => {
  return (
    <StyledFlex ref={ref} {...otherProps}>
      {children}
    </StyledFlex>
  );
});

Flex.propTypes = {};

Flex.defaultProps = {
  display: 'flex',
};

export default Flex;
