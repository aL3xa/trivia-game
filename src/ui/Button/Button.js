import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { StyledButton } from './styled';

const Button = forwardRef((props, ref) => {
  return (
    <StyledButton ref={ref} {...props}>
      {props.children}
    </StyledButton>
  );
});

Button.propTypes = {
  /** Button size */
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  /** Color variant */
  variant: PropTypes.oneOf([
    'primary',
    'secondary',
    'error',
    'warning',
    'success',
    'info',
  ]),
  children: PropTypes.node,
};

Button.defaultProps = {
  size: 'medium',
  variant: null,
};

export default Button;
