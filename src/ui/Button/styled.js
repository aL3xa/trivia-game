import styled, { css } from 'styled-components';
import { composedStyledSystem } from '../utils';

const baseStyles = ({ theme: { palette } }) => css`
  border: none;
  text-align: center;
  cursor: pointer;
  outline: none;
  transition: background-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out;
  box-shadow: 0px 0px 3px 0px rgba(0, 0, 0, 0.5);

  &:disabled {
    cursor: not-allowed;
  }
  &:hover {
    background-color: ${palette.gray.dark};
    box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.5);
  }
  &:active {
    background-color: ${palette.gray.light};
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.5);
  }
`;

const variantStyles = ({ theme: { palette, colors }, variant }) => {
  if (!variant) {
    return;
  }
  let clr = palette[variant];
  return css`
    color: ${colors.white};
    background-color: ${clr.main};
    &:hover {
      background-color: ${clr.dark};
    }
    &:active {
      background-color: ${clr.light};
    }
  `;
};

const sizeStyles = ({ theme: { fontSizes, radii, space }, size }) =>
  ({
    small: {
      fontSize: fontSizes[0],
      borderRadius: radii[1],
      padding: `${space[1]}px ${space[2]}px`,
    },
    medium: {
      fontSize: fontSizes[2],
      borderRadius: radii[1],
      padding: `${space[2]}px ${space[3]}px`,
    },
    large: {
      fontSize: fontSizes[4],
      borderRadius: radii[1],
      padding: `${space[3]}px ${space[4]}px`,
    },
  }[size]);

export const StyledButton = styled.button(
  baseStyles,
  variantStyles,
  sizeStyles,
  composedStyledSystem
);
