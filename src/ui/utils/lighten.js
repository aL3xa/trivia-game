/**
 * Lighten hex color by percentage
 * @param {string} color color in hex notation (preceding with `#`)
 * @param {number} [percent=0] ammount to lighten in percentage (0-100)
 * @returns {string} lightened color in hex notation
 */
export default function lighten(color, percent = 0) {
  const hex = parseInt(color.slice(1), 16);
  const ammount = Math.round(2.55 * percent);
  const normalize = (x) => {
    const min = 0;
    const max = 255;
    return x > max ? max : x < min ? min : x;
  };

  const r = (hex >> 16) + ammount;
  const b = ((hex >> 8) & 0x00ff) + ammount;
  const g = (hex & 0x0000ff) + ammount;
  const [R, B, G] = [r, b, g].map((x) => normalize(x));

  return `#${(0x1000000 + (G | (B << 8) | (R << 16))).toString(16).slice(1)}`;
}
