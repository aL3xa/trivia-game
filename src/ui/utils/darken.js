import lighten from './lighten';
/**
 * Darken hex color by percentage
 * @param {*} color color in hex notation (preceding with `#`)
 * @param {*} ammount ammount to darken in percentage (0-100)
 * @returns {string} darkened color in hex notation
 */
export default function darken(color, ammount = 0) {
  return lighten(color, -ammount);
}
