import {
  compose,
  space,
  color,
  typography,
  layout,
  flexbox,
  grid,
  position,
  border,
} from 'styled-system';

export default function composedStyledSystem() {
  return compose(
    space,
    color,
    typography,
    layout,
    flexbox,
    grid,
    position,
    border
  );
}
