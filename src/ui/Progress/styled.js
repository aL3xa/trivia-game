import styled, { css } from 'styled-components';
import { composedStyledSystem } from 'ui/utils';

const baseStyles = ({ theme: { palette }, percent, color }) => css`
  height: 5px;
  width: 100%;
  background-color: ${palette.gray.light};

  &::before {
    content: '';
    display: block;
    width: ${percent}%;
    height: 100%;
    background-color: ${palette[color].main};
    transition: width 0.2s linear;
  }
`;

export const StyledProgress = styled.div(baseStyles, composedStyledSystem);
