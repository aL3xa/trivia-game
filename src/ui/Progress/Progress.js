import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { StyledProgress } from './styled';

const Progress = forwardRef((props, ref) => {
  return <StyledProgress ref={ref} {...props} />;
});

Progress.propTypes = {
  /** Color variant */
  color: PropTypes.oneOf([
    'primary',
    'secondary',
    'error',
    'warning',
    'success',
    'info',
  ]),
  /** Percent complete */
  percent: (props, propName, componentName) => {
    const percent = props[propName];
    const errorMsg = (message) => {
      throw new Error(
        `Invalid prop "${propName}" in "${componentName}": ${message}`
      );
    };
    if (percent === undefined) {
      errorMsg(`is required`);
    }
    if (typeof percent !== 'number') {
      errorMsg(`not a number`);
    }
    if (percent < 0 || percent > 100) {
      errorMsg(`should be between 0 and 100`);
    }
  },
};

Progress.defaultProps = {
  color: 'primary',
};

export default Progress;
