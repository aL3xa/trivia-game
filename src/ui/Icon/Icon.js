import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { StyledSvg } from './styled';

const Icon = forwardRef(({ size, children, ...otherProps }, ref) => {
  return (
    <StyledSvg
      xmlns="http://www.w3.org/2000/svg"
      width={`${size}px`}
      height={`${size}px`}
      viewBox={`0 0 ${size} ${size}`}
      ref={ref}
      {...otherProps}
    >
      {children}
    </StyledSvg>
  );
});

Icon.propTypes = {
  /** Icon size */
  size: PropTypes.number,
};
Icon.defaultProps = {
  size: 24,
};

export default Icon;
