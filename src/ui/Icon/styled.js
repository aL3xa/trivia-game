import styled from 'styled-components';
import { composedStyledSystem } from '../utils';

export const StyledSvg = styled.svg(composedStyledSystem);
