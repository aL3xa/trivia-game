import { lighten, darken } from 'ui/utils';

export function grayVariants(gray) {
  return {
    light: lighten(gray, 2.5),
    main: gray,
    dark: darken(gray, 5),
  };
}

export function colorVariantStyles(color) {
  return {
    main: color,
    light: lighten(color, 10),
    dark: darken(color, 10),
  };
}

export function variantBuilder(variants, builderFn) {
  return Object.fromEntries(
    Object.keys(variants).map((variant) => [
      variant,
      builderFn(variants[variant]),
    ])
  );
}
