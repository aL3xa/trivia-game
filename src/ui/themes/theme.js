import { variantBuilder, colorVariantStyles, grayVariants } from './palette';

const space = [0, 4, 8, 16, 32, 64];
const fontSizes = [12, 14, 16, 20, 24, 32];
const baseColors = {
  black: '#000',
  white: '#fff',
  gray: '#efefef',
};
const colorVariants = {
  primary: '#1976d2',
  secondary: '#dc004e',
  error: '#f44336',
  warning: '#ff9800',
  success: '#4caf50',
  info: '#2196f3',
};
const colors = {
  ...baseColors,
  ...colorVariants,
};
const radii = [0, 2, 4];
const breakpoints = ['40em', '52em', '64em'];

const baseTheme = {
  space,
  fontSizes,
  colors,
  palette: {
    gray: grayVariants(baseColors.gray),
    ...variantBuilder(colorVariants, colorVariantStyles),
  },
  radii,
  breakpoints,
};
const theme = {
  ...baseTheme,
  color: baseColors.black,
  bg: baseColors.white,
};

export const lightTheme = {
  ...baseTheme,
  color: baseColors.black,
  bg: '#FFF9F9',
  colors: {
    ...baseTheme.colors,
    gray: '#E9E6E6',
  },
  palette: {
    ...baseTheme.palette,
    gray: grayVariants('#E9E6E6'),
  },
};

export const darkTheme = {
  ...baseTheme,
  color: '#EEEEFE',
  bg: '#333336',
};

export default theme;
