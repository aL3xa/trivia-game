import styled from 'styled-components';
import { composedStyledSystem } from 'ui/utils';

const variantStyles = ({ theme: { fontSizes }, variant }) =>
  ({
    h1: { fontSize: fontSizes[5] },
    h2: { fontSize: fontSizes[4] },
    h3: { fontSize: fontSizes[3] },
    h4: { fontSize: fontSizes[2] },
    h5: { fontSize: fontSizes[1] },
    h6: { fontSize: fontSizes[0] },
  }[variant]);

const BaseText = styled.div(variantStyles);

export const StyledText = styled(BaseText)(composedStyledSystem);
