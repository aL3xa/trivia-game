import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { StyledText } from './styled';

const Text = forwardRef(({ children, ...otherProps }, ref) => {
  return (
    <StyledText ref={ref} {...otherProps}>
      {children}
    </StyledText>
  );
});

Text.propTypes = {
  variant: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']),
};

Text.defaultProps = {
  variant: null,
};

export default Text;
