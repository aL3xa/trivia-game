import React, { forwardRef } from 'react';
import { StyledBox } from './styled';

const Box = forwardRef(({ children, ...otherProps }, ref) => {
  return (
    <StyledBox ref={ref} {...otherProps}>
      {children}
    </StyledBox>
  );
});

Box.propTypes = {};

Box.defaultProps = {
  display: 'block',
  position: 'relative',
};

export default Box;
