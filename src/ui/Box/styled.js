import styled from 'styled-components';
import { composedStyledSystem } from '../utils';

export const StyledBox = styled.div(composedStyledSystem);
