import { QUESTION_CATEGORY_DEFAULT_OPTIONS, QUIZ_API_BASE_URL } from 'consts';

const API_QUESTIONS_URL = `${QUIZ_API_BASE_URL}/api.php`;
const API_CATEGORIES_URL = `${QUIZ_API_BASE_URL}/api_category.php`;

export async function getQuestions(params) {
  const url = new URL(API_QUESTIONS_URL);
  url.search = new URLSearchParams(params);
  const response = await fetch(url).then((response) => response.json());
  if (response.response_code !== 0) {
    throw new Error(response.response_code);
  }
  if (!response.results || response.results.length === 0) {
    throw new Error();
  }
  return response.results;
}

export async function getCategories() {
  const url = new URL(API_CATEGORIES_URL);
  const response = await fetch(url).then((response) => response.json());
  if (!response || response.trivia_categories?.length === 0) {
    throw new Error();
  }
  const results = response.trivia_categories.map(({ id, name }) => [
    id,
    { text: name, value: id },
  ]);
  results.push([
    QUESTION_CATEGORY_DEFAULT_OPTIONS[''].value,
    QUESTION_CATEGORY_DEFAULT_OPTIONS[''],
  ]);
  return Object.fromEntries(results);
}

export default {
  getQuestions,
  getCategories,
};
