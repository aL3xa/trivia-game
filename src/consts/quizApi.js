export const {
  REACT_APP_QUIZ_API_BASE_URL: QUIZ_API_BASE_URL = 'https://opentdb.com',
} = process.env;
