export const ASYNC_STATUS = {
  idle: 'idle',
  success: 'success',
  error: 'error',
  loading: 'loading',
};

export const APP_TITLE = 'Trivia Challenge';
export const HOME_PAGE_TITLE = 'Home';
export const SETTINGS_PAGE_TITLE = 'Settings';
export const QUIZ_PAGE_TITLE = 'Quiz';
export const QUIZ_RESULTS_PAGE_TITLE = 'Results';
