export const QUESTION_TYPE_OPTIONS = {
  '': {
    text: 'any',
    value: '',
  },
  boolean: {
    text: 'true or false',
    value: 'boolean',
  },
  multiple: {
    text: 'multiple choice',
    value: 'multiple',
  },
};

export const QUESTION_DIFFICULTY_OPTIONS = {
  '': {
    text: 'any',
    value: '',
  },
  easy: {
    text: 'easy',
    value: 'easy',
  },
  medium: {
    text: 'medium',
    value: 'medium',
  },
  hard: {
    text: 'hard',
    value: 'hard',
  },
};

export const QUESTION_CATEGORY_DEFAULT_OPTIONS = {
  '': {
    text: 'any',
    value: '',
  },
};
