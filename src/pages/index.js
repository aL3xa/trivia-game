export { default as HomePage } from './HomePage';
export { default as QuizPage } from './QuizPage';
export { default as SettingsPage } from './SettingsPage';
