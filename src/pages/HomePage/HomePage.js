import React from 'react';
import { Link } from 'react-router-dom';
import { useSettingsContext } from 'hooks';
import { QUESTION_DIFFICULTY_OPTIONS, QUESTION_TYPE_OPTIONS } from 'consts';
import { Flex, IconButton, Button, Text } from 'ui';
import { Settings as SettingsIcon } from 'ui/icons';

function HomePage() {
  const {
    state: {
      quiz: { amount, type, difficulty, category },
      questionCategories,
    },
  } = useSettingsContext();

  return (
    <>
      <Link to="/settings">
        <IconButton position="absolute" top={0} left={0} m={3}>
          <SettingsIcon />
        </IconButton>
      </Link>

      <Flex
        height="100%"
        alignItems="center"
        flexDirection="column"
        justifyContent="space-around"
        textAlign="center"
      >
        <Text variant="h1" my={3}>
          Welcome to the Trivia Challenge!
        </Text>

        <Text my={2} px={[0, 3, 5]}>
          You will be presented with {amount}{' '}
          {difficulty === ''
            ? ''
            : QUESTION_DIFFICULTY_OPTIONS[difficulty].text}{' '}
          {type === '' ? '' : QUESTION_TYPE_OPTIONS[type].text} questions
          {category === ''
            ? ''
            : ` from "${questionCategories.data[category].text}" category`}
          .
        </Text>

        <Text my={2}>Can you score 100%?</Text>

        <Link to="/quiz">
          <Button variant="primary">Begin</Button>
        </Link>
      </Flex>
    </>
  );
}

export default HomePage;
