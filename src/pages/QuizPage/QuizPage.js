import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import {
  QuestionCard,
  QuestionAnswers,
  QuizResults,
  ConfirmPageLeave,
  QuizError,
} from 'components';
import { Loading, Progress, IconButton } from 'ui';
import { Close as QuitIcon } from 'ui/icons';
import { useQuizContext, useQuizActions, useSettingsContext } from 'hooks';
import { ASYNC_STATUS } from 'consts';

function QuizPage() {
  const {
    init: resetQuizData,
    start: startQuiz,
    answerQuestion,
    goToNextQuestion,
  } = useQuizActions();
  const {
    state: {
      questions,
      status,
      error,
      questionIndex,
      isFinished,
      score,
      answers,
    },
  } = useQuizContext();
  const {
    state: {
      quiz: { showAnswerFeedback },
    },
  } = useSettingsContext();
  const [shouldRender, setShouldRender] = useState(false);

  function handleAnswer(answerData) {
    answerQuestion(answerData);
  }

  function handlePlayAgain() {
    startQuiz();
  }

  useEffect(() => {
    setShouldRender(status === ASYNC_STATUS.success && !!questions);

    return () => {
      setShouldRender(false);
    };
  }, [questions, status]);

  useEffect(() => {
    startQuiz();

    return () => {
      resetQuizData();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (status === ASYNC_STATUS.loading) {
    return <Loading />;
  }

  if (status === ASYNC_STATUS.success) {
    if (isFinished) {
      return (
        <QuizResults
          score={score}
          questions={questions}
          answers={answers}
          onPlayAgain={handlePlayAgain}
        />
      );
    } else {
      const currentQuestion = questions[questionIndex];
      return (
        <>
          <ConfirmPageLeave
            when={!isFinished}
            message={'Quit the Trivia Challenge?'}
          />
          <Link to="/">
            <IconButton
              display={['none', 'flex', 'flex']}
              position="fixed"
              top={0}
              right={0}
              m={3}
            >
              <QuitIcon />
            </IconButton>
          </Link>
          <QuestionCard key={questionIndex} {...currentQuestion}>
            <QuestionAnswers
              {...currentQuestion}
              onAnswer={handleAnswer}
              showCorrect={showAnswerFeedback}
              onNextQuestion={goToNextQuestion}
              shouldRender={shouldRender}
            />
            <Progress percent={(answers.length / questions.length) * 100} />
          </QuestionCard>
        </>
      );
    }
  }

  if (status === ASYNC_STATUS.error || error) {
    return <QuizError error={error} />;
  }
  return null;
}

export default QuizPage;
