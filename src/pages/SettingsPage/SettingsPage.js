import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSettingsContext, useSettingsActions } from 'hooks';
import {
  QUESTION_DIFFICULTY_OPTIONS,
  QUESTION_TYPE_OPTIONS,
  QUESTION_CATEGORY_DEFAULT_OPTIONS,
} from 'consts';
import { ChevronLeft as ChevronLeftIcon } from 'ui/icons';
import { Fieldset, InputGroup, Legend, Label, Input, Select } from './styled';
import { IconButton, Text, Flex, Box } from 'ui';

function SettingsPage() {
  const {
    state: { quiz, app, questionCategories },
  } = useSettingsContext();

  const { save: saveSettings, getCategories } = useSettingsActions();

  const handleSettingsChange = (section) => (evt) => {
    const {
      target: { name, value, type, checked },
    } = evt;
    saveSettings({
      [section]: {
        [name]: type === 'checkbox' ? checked : value,
      },
    });
  };

  useEffect(() => {
    if (questionCategories.data === null) {
      getCategories();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [questionCategories]);

  return (
    <>
      <Link to="/">
        <IconButton position="absolute" top={0} left={0} m={3}>
          <ChevronLeftIcon></ChevronLeftIcon>
        </IconButton>
      </Link>

      <Flex height="100%" width={['100%', '90%', '80%']} flexDirection="column">
        <Text variant="h1" textAlign="center" my={3}>
          Settings
        </Text>

        <Box mt={4}>
          <Fieldset name="quiz">
            <Legend>Quiz settings</Legend>

            <InputGroup>
              <Label htmlFor="amount">Number of questions</Label>
              <Input
                min={1}
                max={50}
                name="amount"
                type="number"
                id="amount"
                defaultValue={quiz.amount}
                onChange={handleSettingsChange('quiz')}
              />
            </InputGroup>

            <InputGroup>
              <Label htmlFor="type">Question type</Label>
              <Select
                name="type"
                id="type"
                defaultValue={quiz.type}
                onChange={handleSettingsChange('quiz')}
              >
                {Object.values(QUESTION_TYPE_OPTIONS).map(({ value, text }) => (
                  <option key={value} value={value}>
                    {text}
                  </option>
                ))}
              </Select>
            </InputGroup>

            <InputGroup>
              <Label htmlFor="difficulty">Difficulty</Label>
              <Select
                name="difficulty"
                id="difficulty"
                defaultValue={quiz.difficulty}
                onChange={handleSettingsChange('quiz')}
              >
                {Object.values(QUESTION_DIFFICULTY_OPTIONS).map(
                  ({ value, text }) => (
                    <option key={value} value={value}>
                      {text}
                    </option>
                  )
                )}
              </Select>
            </InputGroup>

            <InputGroup>
              <Label htmlFor="category">Category</Label>
              <Select
                name="category"
                id="category"
                defaultValue={quiz.category}
                onChange={handleSettingsChange('quiz')}
                maxWidth={150}
              >
                {Object.values(
                  questionCategories.data || QUESTION_CATEGORY_DEFAULT_OPTIONS
                ).map(({ value, text }) => (
                  <option key={value} value={value}>
                    {text}
                  </option>
                ))}
              </Select>
            </InputGroup>

            <InputGroup>
              <Label htmlFor="showAnswerFeedback">Show answer feedback</Label>
              <Input
                type="checkbox"
                name="showAnswerFeedback"
                id="showAnswerFeedback"
                defaultChecked={quiz.showAnswerFeedback}
                onChange={handleSettingsChange('quiz')}
              />
            </InputGroup>
          </Fieldset>

          <Fieldset mt={3}>
            <Legend>App settings</Legend>

            <InputGroup>
              <Label htmlFor="darkTheme">Dark theme</Label>
              <Input
                type="checkbox"
                name="darkTheme"
                id="darkTheme"
                defaultChecked={app.darkTheme}
                onChange={handleSettingsChange('app')}
              />
            </InputGroup>
          </Fieldset>
        </Box>
      </Flex>
    </>
  );
}

export default SettingsPage;
