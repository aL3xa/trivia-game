import styled, { css } from 'styled-components';
import { Box } from 'ui';
import { composedStyledSystem } from 'ui/utils';

/* Form */
export const Form = styled.form(composedStyledSystem);

export const Fieldset = styled.fieldset(
  ({ theme: { radii } }) => css`
    border-radius: ${radii[2]}px;
  `,
  composedStyledSystem
);

export const Legend = styled.legend(
  ({ theme: { color, bg, space } }) => css`
    background-color: ${color};
    color: ${bg};
    padding: ${space[1]}px ${space[2]}px;
    border-radius: 3px;
  `,
  composedStyledSystem
);

export const InputGroup = styled(Box)(
  ({ theme: { space } }) => css`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: ${space[1]}px 0;
  `,
  composedStyledSystem
);

export const Label = styled.label(
  css`
    cursor: pointer;
  `,
  composedStyledSystem
);

export const commonInputStyles = ({
  theme: { bg, color, palette, space },
}) => css`
  color: ${color};
  background-color: ${bg};
  padding: ${space[1]}px;
  border-radius: 3px;
  border: 2px solid ${palette.gray.main};
  outline: none;
  transition: all 0.1s linear;
  text-align: center;

  &[type='checkbox'] {
    cursor: pointer;
  }

  &:hover {
    border-color: ${palette.gray.dark};
  }

  &:focus {
    border-color: ${palette.primary.light};
  }
`;

export const Input = styled.input(commonInputStyles, composedStyledSystem);

export const Select = styled.select(
  css`
    cursor: pointer;
  `,
  commonInputStyles,
  composedStyledSystem
);
