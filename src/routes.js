import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { HomePage, QuizPage, SettingsPage } from 'pages';
import { QuizProvider } from 'contexts/QuizContext';

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/">
        <HomePage />
      </Route>
      <Route exact path="/quiz">
        <QuizProvider>
          <QuizPage />
        </QuizProvider>
      </Route>
      <Route exact path="/settings">
        <SettingsPage />
      </Route>

      <Route>
        <Redirect to="/" />
      </Route>
    </Switch>
  );
}
