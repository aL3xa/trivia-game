import React, { createContext, useEffect, useReducer } from 'react';
import deepmerge from 'deepmerge';
import {
  SETTINGS_DEFAULT_QUIZ_AMOUNT,
  SETTINGS_DEFAULT_QUIZ_DIFFICULTY,
  SETTINGS_DEFAULT_QUIZ_TYPE,
  SETTINGS_DEFAULT_QUIZ_CATEGORY,
  SETTINGS_DEFAULT_QUIZ_SHOW_CORRECT_ANSWERS,
  SETTINGS_ACTION_SAVE,
  SETTINGS_ACTION_GET_CATEGORIES,
  ASYNC_STATUS,
} from 'consts';

const initialState = {
  quiz: {
    amount: SETTINGS_DEFAULT_QUIZ_AMOUNT,
    type: SETTINGS_DEFAULT_QUIZ_TYPE,
    category: SETTINGS_DEFAULT_QUIZ_CATEGORY,
    difficulty: SETTINGS_DEFAULT_QUIZ_DIFFICULTY,
    showAnswerFeedback: SETTINGS_DEFAULT_QUIZ_SHOW_CORRECT_ANSWERS,
  },
  app: {
    darkTheme: false,
  },
  questionCategories: {
    data: null,
    status: ASYNC_STATUS.idle,
  },
};

const getSavedSettings = () => {
  const savedSettings = JSON.parse(localStorage.getItem('settings')) || {};
  return deepmerge(initialState, savedSettings);
};

export const SettingsContext = createContext();

const settingsReducer = (state, { type, payload }) => {
  switch (type) {
    case SETTINGS_ACTION_SAVE: {
      return deepmerge(state, payload);
    }
    case SETTINGS_ACTION_GET_CATEGORIES: {
      const { data, status } = payload;
      return {
        ...state,
        questionCategories: {
          data,
          status,
        },
      };
    }
    default: {
      return state;
    }
  }
};

export const SettingsProvider = ({ children }) => {
  const [state, dispatch] = useReducer(
    settingsReducer,
    initialState,
    getSavedSettings
  );

  useEffect(() => {
    localStorage.setItem('settings', JSON.stringify(state));
  }, [state]);

  return (
    <SettingsContext.Provider value={{ state, dispatch }}>
      {children}
    </SettingsContext.Provider>
  );
};
export const SettingsConsumer = SettingsContext.Consumer;

export default SettingsContext;
