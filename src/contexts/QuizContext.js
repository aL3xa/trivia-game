import React, { createContext, useReducer } from 'react';
import {
  QUIZ_ACTION_INIT,
  QUIZ_ACTION_ERROR,
  QUIZ_ACTION_GET_QUESTIONS,
  QUIZ_ACTION_ANSWER_QUESTION,
  QUIZ_ACTION_GOTO_NEXT_QUESTION,
  ASYNC_STATUS,
} from 'consts';

const initialState = {
  questions: null,
  status: ASYNC_STATUS.idle,
  error: null,
  isFinished: null,
  questionIndex: 0,
  answers: [],
  score: 0,
};

const quizReducer = (state, { type, payload }) => {
  switch (type) {
    case QUIZ_ACTION_INIT: {
      return initialState;
    }
    case QUIZ_ACTION_GET_QUESTIONS: {
      const { questions, status } = payload;
      return {
        ...state,
        questions,
        status,
      };
    }
    case QUIZ_ACTION_ERROR: {
      const { error, status } = payload;
      return {
        ...state,
        status,
        error,
      };
    }
    case QUIZ_ACTION_ANSWER_QUESTION: {
      const { answer, isCorrect } = payload;
      const score = isCorrect ? state.score + 1 : state.score;
      return {
        ...state,
        score,
        answers: [
          ...state.answers,
          {
            answer,
            isCorrect,
          },
        ],
      };
    }
    case QUIZ_ACTION_GOTO_NEXT_QUESTION: {
      const questionIndex = state.questionIndex + 1;
      const isFinished =
        state.questions !== null && questionIndex >= state.questions.length;
      if (isFinished) {
        return {
          ...state,
          isFinished,
        };
      }
      return {
        ...state,
        questionIndex,
      };
    }
    default: {
      return state;
    }
  }
};

export const QuizContext = createContext();

export const QuizProvider = ({ children }) => {
  const [state, dispatch] = useReducer(quizReducer, initialState);
  return (
    <QuizContext.Provider value={{ state, dispatch }}>
      {children}
    </QuizContext.Provider>
  );
};
export const QuizConsumer = QuizContext.Consumer;

export default QuizContext;
