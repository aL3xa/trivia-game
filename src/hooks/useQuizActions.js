import { getQuestions as apiGetQuestions } from 'services/quizApi';
import {
  QUIZ_ACTION_INIT,
  QUIZ_ACTION_ERROR,
  QUIZ_ACTION_GET_QUESTIONS,
  QUIZ_ACTION_ANSWER_QUESTION,
  QUIZ_ACTION_GOTO_NEXT_QUESTION,
  ASYNC_STATUS,
} from 'consts';
import { useQuizContext, useSettingsContext } from 'hooks';

export default function useQuizActions() {
  const { dispatch } = useQuizContext();
  const {
    state: { quiz: quizSettings },
  } = useSettingsContext();

  const init = () => {
    dispatch({ type: QUIZ_ACTION_INIT });
  };

  const getQuestions = async () => {
    dispatch({
      type: QUIZ_ACTION_GET_QUESTIONS,
      payload: { status: ASYNC_STATUS.loading },
    });
    try {
      const { amount, type, category, difficulty } = quizSettings;
      const questions = await apiGetQuestions({
        amount,
        type,
        category,
        difficulty,
      });
      dispatch({
        type: QUIZ_ACTION_GET_QUESTIONS,
        payload: {
          status: ASYNC_STATUS.success,
          questions,
        },
      });
    } catch (error) {
      dispatch({
        type: QUIZ_ACTION_ERROR,
        payload: {
          status: ASYNC_STATUS.error,
          error: error.message,
        },
      });
    }
  };

  const start = async () => {
    dispatch({ type: QUIZ_ACTION_INIT });
    await getQuestions();
  };

  const answerQuestion = ({ answer, isCorrect }) => {
    dispatch({
      type: QUIZ_ACTION_ANSWER_QUESTION,
      payload: {
        answer,
        isCorrect,
      },
    });
  };

  const goToNextQuestion = () => {
    dispatch({
      type: QUIZ_ACTION_GOTO_NEXT_QUESTION,
    });
  };

  return {
    init,
    start,
    getQuestions,
    answerQuestion,
    goToNextQuestion,
  };
}
