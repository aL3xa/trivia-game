import {
  SETTINGS_ACTION_SAVE,
  SETTINGS_ACTION_GET_CATEGORIES,
  ASYNC_STATUS,
} from 'consts';
import { useSettingsContext } from 'hooks';
import { getCategories as apiGetCategories } from 'services/quizApi';

export default function useSettingsActions() {
  const { dispatch } = useSettingsContext();

  const save = (data) => {
    dispatch({ type: SETTINGS_ACTION_SAVE, payload: data });
  };

  const getCategories = async () => {
    dispatch({
      type: SETTINGS_ACTION_GET_CATEGORIES,
      payload: { status: ASYNC_STATUS.loading },
    });
    try {
      const data = await apiGetCategories();
      dispatch({
        type: SETTINGS_ACTION_GET_CATEGORIES,
        payload: { data, status: ASYNC_STATUS.success },
      });
    } catch {
      dispatch({
        type: SETTINGS_ACTION_GET_CATEGORIES,
        payload: { status: ASYNC_STATUS.error },
      });
    }
  };

  return {
    save,
    getCategories,
  };
}
