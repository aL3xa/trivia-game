import { useEffect, useState } from 'react';
import useSettingsContext from './useSettingsContext';
import { lightTheme, darkTheme } from 'ui/themes';

export default function useTheme() {
  const {
    state: {
      app: { darkTheme: isDarkTheme },
    },
  } = useSettingsContext();
  const [theme, setTheme] = useState(lightTheme);

  useEffect(() => {
    setTheme(isDarkTheme ? darkTheme : lightTheme);
  }, [isDarkTheme]);

  return theme;
}
