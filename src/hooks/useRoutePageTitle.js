import { APP_TITLE, QUIZ_PAGE_TITLE, SETTINGS_PAGE_TITLE } from 'consts';
import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

export default function useRoutePageTitle() {
  const { pathname } = useLocation();
  const buildPageTitle = (page) => {
    let title = APP_TITLE;
    if (page) {
      title = `${title} - ${page}`;
    }
    return title;
  };
  const routeMapping = {
    '/': buildPageTitle(),
    '/quiz': buildPageTitle(QUIZ_PAGE_TITLE),
    '/settings': buildPageTitle(SETTINGS_PAGE_TITLE),
  };

  useEffect(() => {
    document.title = routeMapping[pathname] || APP_TITLE;
  }, [pathname, routeMapping]);
}
