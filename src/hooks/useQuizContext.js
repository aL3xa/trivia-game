import { useContext } from 'react';
import { QuizContext } from 'contexts';

export default function useQuizContext() {
  return useContext(QuizContext);
}
