import React from 'react';
import { ThemeProvider } from 'styled-components';
import Routes from './routes';
import { Container, PageWrapper, GlobalStyles } from 'components';
import { useRoutePageTitle, useTheme } from 'hooks';

function App() {
  useRoutePageTitle();
  const theme = useTheme();

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <Container>
        <PageWrapper>
          <Routes />
        </PageWrapper>
      </Container>
    </ThemeProvider>
  );
}

export default App;
